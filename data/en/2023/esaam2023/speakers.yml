items:
  - name: "Rolf Riemenschneider"
    anchor: rolf_riemenschneider
    title: "European Commission, Keynote"
    img: Rolf_Riemenschneider.jpg
    bio: | 
      <p><strong>Head of Sector "Internet of Things", DG CONNECT, European Commission</strong></p>
      <p>Rolf Riemenschneider drives the research and innovation agenda under the programme Horizon Europe where he coordinates 
      the R&I strategy related to the strategy on Cloud-Edge Computing and Internet of Things. Since 2015, he coordinates 
      the cross-cutting activities  related to Internet of Things in Horizon 2020  across different policy streams 
      with DG AGRI, DG ENER, DG MOVE and DG GROW.</p>
      <p>As senior expert, he leads the research, innovation and deployment of the next generation of the IoT with strong 
      computing capacity at the edge exploring features like device virtualisation, interoperability, real-time, energy 
      efficiency and distributed intelligence and emerging topics like the Industrial Metaverse</p>
      <p>In the context of the key horizontal activities related to achieving the targets of the Green Deal, he works together 
      with DG ENERGY on the Digitisation of the Energy System with the recently published Digitalisation of Energy Action Plan. 
      Actions include data-driven interoperable solutions and products to enable an increased flexibility in energy consumption, 
      foster the integration of renewable energy, and innovative application and services across sectors like home, building, 
      energy and mobility.</p>
      
  - name: "Marco Gonzalez Hierro"
    anchor: marco_gonzalez_hierro
    title: "IKERLAN, Keynote"
    img: MarcoGonzalez_IKERLAN_square.jpg
    bio: | 
      <p>Marco Gonzalez Hierro has MSc in Telecommunications Engineering from the University of the Basque Country (UPV/EHU). 
      He has been working as a researcher at IKERLAN since 2016. He is currently Head of the Information and Communications 
      Technologies (ICT) department, leading +60 researchers focused on IoT, Edge-to-Cloud Continuum, and Machine Learning/Artificial 
      Intelligence research lines with over 2.5M€ portfolio in R&D Projects for companies. In addition, he has extensive experience as 
      a project manager and software developer. He also has expertise in Edge/Cloud systems architecture, mobile connectivity, and 
      cybersecurity. He has participated in numerous technology transfer projects for industry, mainly in the energy, railway, and elevation sectors.</p>
      
  - name: "Giovanni Rimassa"
    anchor: giovanni_rimassa
    title: "MARTEL, Sponsor"
    img: giovanni_rimassa.jpg
    bio: |
      <p>Dr. Giovanni Rimassa is Chief Innovation Officer at Martel, supporting business development, market positioning and commercialisation activities, 
      as well as leading initiatives around Artificial Intelligence and Computing Continuum. Giovanni holds a Ph.D. in Information Engineering (Università 
      di Parma, Italy), with a Thesis on Runtime Support for Distributed Multi-Agent Systems that kickstarted the open-source, most popular implementation 
      of IEEE FIPA standard in 2001. Open Source Software accompanied him during his 20 year professional path between academia and industry, from the multi-agent 
      middleware beginning, over to an Eclipse-based iBPMS platform, to the current Martel offering for IoT and Smart Cities.</p>
  
  # paper 01
  - name: "Ioannis Chochliouros"
    anchor:	ioannis_chochliouros
    title: "Hellenic Telecommunications Organisation"
    img: 01-Ioannis_Chochliouros.jpg
    bio: |
      <p>Dr. Ioannis P. CHOCHLIOUROS graduated from the Dept. of Electrical Engineering of the Polytechnic School of Aristotle University of Thessaloniki, 
      Greece, holding also a M.Sc. (D.E.A.) and a Ph.D. (Doctorat) from the University Pierre et Marie Curie (Paris VI), France. His practical experience 
      as an engineer has been mainly in Telecommunications, as well as in various constructive projects in Greece and the wider Balkan area. Since 1997 
      he has worked at the Competition Department and as an engineer-consultant of the Chief Technical Officer of the Hellenic Telecommunications Organisation 
      S.A. (OTE). He has been very strongly involved in major OTE’s national and international business activities, as a specialist-consultant for technical 
      and regulatory affairs, especially for the evaluation and the adoption of innovative e-Infrastructures and e-Services in Greece and abroad. He has also 
      served as the Head of Technical Regulations Dept. of OTE’s Division for Standardisation and Technical Regulations, representing OTE in international 
      standardisation for a and he has been involved in an enormous variety of issues regarding European and international standardisation, with emphasis 
      on modern technologies. In addition, he has also worked as an independent consultant in the scope of several European and/or international research 
      and business studies. Since 2005, he is the Head of OTE’s Fixed Network R&D Programs Section and has been involved in different national, European 
      and international R&D projects and market-oriented activities, many of which have received international awards. During his professional career, 
      he has participated either as coordinator and/or as a scientist-researcher in more than 75 European and national research programs, some of which 
      have received distinctive awards by the European Commission. He is author/co-author of three international books and he has published more than 290 
      distinct scientific or business papers/reports in the international literature (book chapters and articles in magazines, journals and conferences 
      proceedings), especially for technical, business and regulatory options arising from innovative e-Infrastructures and e-Services. He is an expert in 
      project management activities with an extensive experience in EU-funded projects where he has very successfully exercised coordinator’s duties 
      (e.g., 5G-PPP 5G ESSENCE, 5G-PPP SESAME, Privacy-Flag, LiveCity and D-SPACE). He is also an active participant of various international and national 
      associations, both of scientific and business nature. Dr. Chochliouros has also performed an extended educational activity in Greece and in France, 
      in cooperation with Universities and other high-level Institutes, covering a broad variety of issues in the scope of modern e-communications. 
      Recently he has received the distinctive award of being a member of the IPv6 Hall of Fame.</p>
      
  # paper 02
  - name: "Borja Arroyo Galende"
    anchor: borja_arroyo_galende
    title: "Universidad Politécnica de Madrid"
    img: 02-Borja_Arroyo_Galende.jpg
    bio: |
      <p>Borja Arroyo Galende received the degree in forestry engineering from the Universidad Politécnica de Madrid (UPM) in 2019,
      and the degree in computer engineering from the Universidad Nacional de Educación a Distancia (UNED) in 2020. He then received 
      the master’s degree in data science and engineering from UNED in 2021. He is currently pursuing his PhD studies, focusing mainly 
      on generative AI on tabular data and federated learning. At the same time, he works in the GATV research group at the UPM as a 
      software developer. His main interests include data driven software systems applied to health and environmental disciplines.</p>
  # paper 03
  - name: "Peter Bednar"
    anchor: peter_bednar 
    title: "Technical University of Košice"
    img: 03-Peter_Bednar.jpg
    bio: |
      <p>Peter Bednár is an associate professor at the Technical University of Košice. He successfully defended his habilitation 
      thesis in 2022, focusing on semantics in information retrieval. He participated in many EU research projects designing and 
      implementing distributed architectures for industry, business and government. His research interest includes natural 
      language processing, machine learning, information extraction and information retrieval. He is also engaged with 
      Dr.Evidence Inc. developing distributed machine-learning pipelines for processing vast amounts of medical papers and drug 
      labels.</p>
  # paper 04
  - name: "Francesco Lumpp"
    anchor: francesco_lumpp 
    title: "University of Verona"
    img: 04-Francesco_Lumpp.jpg
    bio: |
      <p>Francesco Lumpp is currently working toward the PhD degree with the Department of Engineering for Innovation Medicine, 
      University of Verona, Verona, Italy. His research activity focuses on development and optimization of software for 
      edge-cloud computing.</p>
  # paper 05 & 06
  - name: "Davide Taibi"
    anchor: davide_taibi 
    title: "University of Oulu"
    img: 05-06-Davide_Taibi.jpg
    bio: |
      <p>Davide Taibi is full Professor at the University of Oulu (Finland) and Associate Professor at the Tampere University 
      (Finland) where he heads the M3S Cloud research group. His research is mainly focused on the migration to cloud-native 
      technologies. He is investigating processes, and techniques for developing Cloud Native applications, identifying 
      cloud-native specific patterns and anti-patterns, and methods to prevent the degradation. He is member of the 
      International Software Engineering Network (ISERN) from 2018. Formerly, he worked at the Free University of Bolzano, 
      Technical University of Kaiserslautern, Germany, Fraunhofer IESE - Kaiserslautern, Germany, and Università degli Studi 
      dell’Insubria, Italy. In 2011 he was one of the co-founders of Opensoftengineering s.r.l., a spin-off company of the 
      Università degli Studi dell’Insubria.</p>
  # paper 07
  - name: "Michael Tsechelidis"
    anchor: michael_tsechelidis 
    title: "University of Macedonia"
    img: 07-Michael_Tsechelidis.jpg
    bio: |
      <p>Michail Tsechelidis is currently pursuing the B.Sc. degree from the Department of Applied Informatics from the 
      University of Macedonia, Greece. He just finalized his thesis titled "Developing Distributed Systems with Modular 
      Monoliths and Microservices" which draw him into the research. His research interests include Software Architecture, 
      Requirement Analysis and the Software Development Lifecycle (SDLC) of: Distributed Systems, Internet of Things (IoT), 
      Microservices and Modular Monoliths.</p>
  # paper 08
  - name: "Feryal Fulya Horozal"
    anchor: feryal_fulya_horozal
    title: "ATB Bremen"
    img: 08-Feryal_Fulya_Horozal.png
    bio: |
      <p>Fulya Horozal obtained her Ph.D. in computer science from Jacobs University Bremen, Germany. Her early research 
      activities focused on knowledge representation and reasoning techniques for formal systems, where she has tightly 
      collaborated with the German Centre for Artificial Intelligence on developing techniques for formal systems interoperability. 
      She has also gained industry experience in the domain of manufacturing and testing for the automotive sector. She is currently 
      working as a scientific researcher at ATB - Institut für angewandte Systemtechnik Bremen GmbH. She has participated in several 
      EU-projects contributing to the development of methodologies and tools for adaptive systems for critical infrastructures, 
      zero-defect manufacturing in Industry 4.0 and domain-specific languages for hybrid data architectures and containerization. 
      Her recent research activity includes tool support for architectural patterns in a new cloud-native IDE as part of the 
      HORIZON2020 EU-funded SmartCLIDE project. Her current active research areas extend to the digitalization of industrial processes 
      and the validation of AI systems.</p>
  # paper 09
  - name: "Gorka Benguria Elguezabal"
    anchor: gorka_benguria_elguezabal 
    title: "University of Deusto"
    img: 09-Gorka_Benguria_Elguezabal.jpg
    bio: |
      <p>Gorka Benguria Elguezabal is a Software Engineer from the University of Deusto. Gorka has worked since 1998 in the 
      execution and direction of research projects related to information and communication technologies. During this 
      period he has covered various research areas including processes and quality models, development and migration to 
      service-based solutions and, in recent years, migration of complex systems to cloud and edge infrastructures. 
      He has been involved in the application of these technologies in diverse vertical domains such as Health, Energy 
      and Public Administration.</p>
  # paper 10
  - name: "Savidu Dias"
    anchor: savidu_dias
    title: "Solita"
    img: 10-Savidu_Dias.jpg
    bio: |
      <p>Savidu Dias is a Data Engineer at Solita, with a background in Machine Learning, Internet of Things, and Edge Computing. 
      Dias specializes in developing Machine Learning solutions for IoT systems.</p>
  # paper 11
  - name: "Jacopo Marino"
    anchor: jacopo_marino
    title: "Politecnico di Torino"
    img: 11-Jacopo_Marino.jpg
    bio: |
      <p>Jacopo Marino (born in 1998) is a PhD student at the Department of Control and Computer Engineering of Politecnico di Torino, 
      Italy. He received his Bachelor's degree in Computer Engineering in 2020, followed by the M.Sc. in the same discipline in 2022, 
      both from Politecnico di Torino. His research interests focus on the domains of cloud computing and the orchestration of 
      multi-cloud environments.</p>
  # paper 12
  - name: "Olli Saarela"
    anchor: olli_saarela
    title: "VTT"
    img: 12-Olli_Saarela.jpg
    bio: |
      <p>Dr. Olli Saarela, Principal Investigator and Senior Scientist at VTT, has focused his scientific career on development 
      and application of data science for operation and maintenance of industrial processes and machines. This work Dr. Saarela 
      has carried out for power industry at Imatran Voima Oy (Now Fortum Ltd), for pulp and paper industry at Oy Keskuslaboratorio 
      (KCL), and now for a spectrum of industries at VTT. His doctoral dissertation (Tampere University of Technology, 2002) was 
      on diagnosis of process-wide variability in industrial processes. Dr. Saarela's foreign engagements include two years as 
      a Visiting Scientist at Massachusetts Institute of Technology in USA, and two years as a Guest Scientist at OECD Halden 
      Reactor Project in Norway.</p>
      
      
      