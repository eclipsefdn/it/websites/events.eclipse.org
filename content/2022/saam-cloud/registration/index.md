---
title: "Eclipse SAAM on Cloud 2022"
headline: "Eclipse SAAM on Cloud 2022"
subtitle: "Security, AI, Architecture and Modelling for next generation of edge-cloud computing continuum"
tagline: "Ludwigsburg, Germany | October 25, 2022"
date: 2022-10-25T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-saam-2022-event"
hide_breadcrumb: true
container: "container-fluid saam-2022-event"
summary: "The Eclipse SAAM on Cloud conference will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, especially focusing on Security and Privacy, Artificial Intelligence, Architecture, Modelling and related challenges. This event is co-located with EclipseCon 2022"
layout: single
keywords: ["eclipse", "ATB", "CERTH", "ITI", "conference", "research", "SAAM", "Cloud", "Cloud Computing", "Security", "AI", "Architecture", "Modelling", "Modeling"]
links: [[href: "https://eclipsecon.regfox.com/eclipse-saam-on-cloud-2022",text: "Register Now"]]
---

{{< grid/section-container id="how" class="featured-section-row featured-section-row-lighter-bg text-center">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}
# Registration
{{</ grid/div >}}
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="things_to_know" class="featured-section-row featured-section-row-light-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}

## Things to Know
* We recognize that health and safety is an important concern for our attendees right now. For more, please see [EclipseCon Health and Safety](https://www.eclipsecon.org/2022/eclipsecon-health-and-safety).
* We strive to protect the privacy of our attendees. Please see [EclipseCon and Your Privacy](https://www.eclipsecon.org/2022/eclipsecon-privacy) to learn more.
* All registrants must agree to the [Code of Conduct](https://www.eclipsecon.org/2022/eclipse-community-events-code-conduct) as part of the registration process.
* The conference language is English.
* Lunch is provided each day.
* Payment must be made via credit card in euros.
* 19% German VAT is applied to all registration prices.
* For questions about registration (either before or after you register), please send email to [research@eclipse.org](research@eclipse.org).
{{</ grid/div >}}
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="how" class="featured-section-row featured-section-row-lighter-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}
## How to Register
Registration is done on an external site. Clicking on the “Register Now” button will take you to that site. Please read the information in the “Registration Prices” section below to learn about the various pass types **before** you start the registration process.

Once you are at the registration site, please follow these steps to register:

1. Fill in your personal information
1. Choose your pass type
1. If you have a Coupon Code, enter it in the Coupon Code box
1. Complete the rest of the information
1. Be sure to click on “REGISTER” at the end
1. You will receive an email confirmation from the registration system after your registration is complete
{{</ grid/div >}}
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="prices" class="featured-section-row featured-section-row-light-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
	{{< grid/div class="col-md-4 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}
	{{< grid/div class="col-md-16 padding-bottom-20" isMarkdown="true">}}
## Registration Prices
Some of the SAAM on Cloud passes include access to Community Day and Hacker Day, which are part of EclipseCon 2022. To learn more about these days, please see the information below.

Note that 19% German VAT is applied to all registration.
| Pass Type                                                      | Prices     | 
| :---                                                           |      ----: |
| **SAAM on Cloud Pass** (Tuesday 25)                            | €350 + VAT |
| **SAAM on Cloud + Community Day** (Monday 24 and Tuesday 25)   | €425 + VAT |
| **SAAM on Cloud + EclipseCon** ( Tuesday 25 to Thursday 27)    | €595 + VAT |
| **SAAM on Cloud All Access** (Monday 24 to Thursday 27)        | €670 + VAT |
{{</ grid/div >}}
{{< grid/div class="col-md-4 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}

{{< grid/div class="col-md-9 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}
{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="false">}}
   {{< bootstrap/button linkClass="btn-primary btn-wide" href="https://eclipsecon.regfox.com/eclipse-saam-on-cloud-2022">}}Register Now{{</bootstrap/button>}}
{{</ grid/div >}}
	
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="community_day" class="featured-section-row featured-section-row-lighter-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}
## Community Day
Community Day is on Monday, October 24, the day before the main conference starts. Attendance is included with a SAAM on Cloud All Access and SAAM on Cloud + Community Day passes. To learn more, visit the [Community Day page](https://www.eclipsecon.org/2022/community-day). Joining this day will give you the opportunity to meet one of the most active Eclipse communities, the **Eclipse CloudDev Tools Community**.

{{</ grid/div >}}
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="hacker_day" class="featured-section-row featured-section-row-light-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}
## Hacker Day
**NEW this year!** EclipseCon sessions end at 12:00 on Thursday, October 27. After lunch, Hacker Day begins. Hacker Day is open to all EclipseCon attendees. This year, our sponsor SmartCLIDE is organizing its own hackathon to present and demo its extension of Eclipse Theia. SmartCLIDE  provides a development environment that makes it easy to create, compose, test and deploy full-stack services and applications in the cloud. To learn more about SmartCLIDE, visit their [website](https://smartclide.eu/). To learn more about the Hacker Day, visit the [Hacker Day page](https://www.eclipsecon.org/2022/hacker-day).

{{</ grid/div >}}
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="international_travelers" class="featured-section-row featured-section-row-lighter-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}
## International Travelers
If you need a letter from us in order to obtain travel documents, please [email us](research@eclipsecon.org). We can provide a signed letter as an email attachment that confirms your status as an Eclipse SAAM on Cloud registrant or an Eclipse SAAM on Cloud speaker. Here is [an example of a letter](2022_Template_Registrant.pdf).
{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="international_travelers" class="featured-section-row featured-section-row-light-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}
## Questions
If you have questions or need help with registration, please [email us](research@eclipsecon.org).

{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}


