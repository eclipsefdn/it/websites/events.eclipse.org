---
title: "EclipseCon Europe 2019"
type: "eclipsecon"
layout: "section"
description: "Conference for the Eclipse Ecosystem | October 22 - 24, 2019 | Ludwigsburg, Germany"
summary: |
  EclipseCon is the leading conference for developers, architects, and open
  source business leaders to learn about Eclipse technologies, share best
  practices, and more. EclipseCon is our biggest event of the year and connects
  the Eclipse ecosystem and the industry’s leading minds to explore common
  challenges and innovate together on open source runtimes, tools, and
  frameworks for cloud and edge applications, IoT, artificial intelligence,
  connected vehicles and transportation, digital ledger technologies, and much
  more.
---

{{< europe/2019 >}} 

