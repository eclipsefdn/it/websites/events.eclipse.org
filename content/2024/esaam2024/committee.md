---
title: "eSAAM 2024 Organisation Committee"
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">eSAAM 2024 on Data Spaces</h1>
    <h2 class="featured-jumbotron-subtitle text-center">
        4th Eclipse Security, AI, Architecture and Modelling Conference<br/>on Data Spaces
    </h2>
    <h3 class="featured-jumbotron-subtitle text-center">October 22, 2024 | Mainz, Germany</h3>
custom_jumbotron_class: container-fluid
date: 2024-10-22T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
show_featured_footer: false
hide_call_for_action: true
header_wrapper_class: "header-esaam2024-event"
hide_breadcrumb: true
container: "container-fluid esaam-2024-event"
summary: "For its fourth edition, eSAAM 2024 was about data spaces. Come together with industry experts and researchers working on innovative software and systems solutions for data spaces, specifically focusing on security and privacy, artificial intelligence and machine learning, systems and software architecture, modelling and related challenges."
layout: single
main_menu: esaam2024
keywords: ["eclipse", "UOM", "CERTH", "ITI", "conference", "research", "eSAAM", "Dataspace", "Data Space", "Security", "AI", "Architecture", "Modelling", "Modeling"]
links: [ [href: "..", text: "Return Main Page"]  ]
---

{{< grid/section-container id="committee">}}
	{{< events/user_bios event="esaam2024" year="2024" source="committee" imgRoot="/events/2024/esaam2024/images/speakers/" >}}
{{</ grid/section-container >}}