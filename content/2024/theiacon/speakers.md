---
title: "Speakers"
date: 2024-10-17:00:00-04:00
description: "The speakers of TheiaCon 2024."
keywords: ["theiacon", "speakers", "talks", "presentations"]
hide_page_title: false
hide_sidebar: true
hide_breadcrumb: false
layout: "speakers"
---

{{< events/user_bios event="theiacon" year="2024" source="speakers" imgRoot="/events/2024/theiacon/images/speakers/" >}}
